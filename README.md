##Say "hey" to antiSMASH!

I'm Mariana, nice to meet you!

I'm currently pursuing a Master in Bioinformatics and Computational Biology. I'm graduated in Biochemistry with a Masters in Biopharmaceutical Sciences.

This is my first **Google Summer of Code** experience and I'll be working with Kai Blin and Marc Chevrette on antiSMASH.
What is it? Well, [antiSMASH](http://antismash.secondarymetabolites.org/) is a software pipeline for the identification of secondary metabolite biosynthetic clusters from genomic input 
and it allows the prediction of products produced by the identified cluster.


__My project: Improve the prediction for RiPP clusters__

Ribosomally synthesised and post­translationally modified peptides (RiPPs) are a class of secondary metabolites that ​ share a common biosynthetic origin -­ a ​ precursor gene, encoded on the genome of a microorganism. It is translated to a peptide by the ribosome, and then further modified to give the active final product. RiPPs can be divided in more than 20 sub­classes and can have a wide range of biological functions: among others, they can work as antibiotics, nutritional additives or food preservatives. Thus, the use of RiPPs as drugs is increasing and several RiPP derivatives are already in advanced phases of clinical trials. 

In this project, prediction modules for a number of RiPP clusters will be developed. Working together with the mentors, who can provide advice both from the technical as well as scientific side of the project, antiSMASH plug­ins will be created to allow researchers to identify the RiPP precursor gene, identify the cleavage sites of the core peptide and predict further modifications to the peptide. With these plug­ins, we hope to accelerate all the research involving RiPPs and consequently, shorten the time “between the bench and the bed” for possible new drugs. 

Welcome to my GSoC Blog ^^



##[Week 1](https://bitbucket.org/mnave/gsoc-blog/src/a5a1f29d17658a4537a6998aea761531e5c60d0c/posts/Week1.md?at=master&fileviewer=file-view-default)


##[Week 2](https://bitbucket.org/mnave/gsoc-blog/src/8790d6fe7d3543b3a3526369359e5735da170e75/posts/Week2.md?at=master&fileviewer=file-view-default)


##[Week 3](https://bitbucket.org/mnave/gsoc-blog/src/8f63b2d03fe0c3dbc0fb1b8f9855251d3860b62f/posts/Week3.md?at=master&fileviewer=file-view-default)


##[Week 4](https://bitbucket.org/mnave/gsoc-blog/src/c209e89c1467177ac7e1798a10dab238843bc081/posts/Week4.md?at=master&fileviewer=file-view-default)


##[Week 5](https://bitbucket.org/mnave/gsoc-blog/src/c7b1bd196514e9e6ef202f1bed853556774a74c8/posts/Week5.md?at=master&fileviewer=file-view-default)


##[Weeks 6 & 7](https://bitbucket.org/mnave/gsoc-blog/src/11514ee0ca26f2502cdfb43d9831ae208d3ba135/posts/Week6&7.md?at=master&fileviewer=file-view-default)


##[Weeks 8 & 9](https://bitbucket.org/mnave/gsoc-blog/src/4a13bef752f8fed2c4cacf02378f0d24ed3ec2c4/posts/Week8&9.md?at=master&fileviewer=file-view-default)


##[Weeks 10 - 12](https://bitbucket.org/mnave/gsoc-blog/src/02dca81a750609b8dfd6bdb297582bdbe6f20a80/posts/Weeks10-12.md?at=master&fileviewer=file-view-default)


##[antiSMASH plugin summary and closing thoughts](https://bitbucket.org/mnave/gsoc-blog/src/dd9e78b42270f188bb909cbd354f095e9c2f6755/posts/Summary.md?at=master&fileviewer=file-view-default)