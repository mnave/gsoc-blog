Hi again!

GSoC 2016 is almost at the finish line so I decided to write a simplified summary of one of the modules I did for antiSMASH.
But before that, let me start by thanking Google, Open Bioinformatics Foundation, antiSMASH and in particular, to my amazing mentors, Kai Blin and Marc Chevrette. They were both extremely supportive, accessible and always available. Thank you!


So, I worked with two different classes of Ribosomally synthesised and post­translationally modified peptides (RiPPs): Lassopeptides and Thiopeptides. As these last ones were more tricky, lets see the lassos.

So, first of all, how can we create a module in antiSAMSH?
In a simple manner, we can have a scheme like this:

[![pluginscheme.png](https://s3.postimg.org/e5drinolf/pluginscheme.png)](https://postimg.org/image/6cn3qoim7/)

One can take a genomic input - GenBank / EMBL format (recommended) or in FASTA format or input NCBI accession number of desired file. This information will be send to antiSMASH, where according to a set of rules, it will determine which type of cluster is. In my example, let's assume it is a Lassopeptide.
Once a Lassopeptide, "my plugin" will take the necessary information to proceed with the prediction of different features of the peptide. An html ouput is then generated an finally, antiSMASH returns our results in a simple way.

As I wrote in previous posts, these RiPPs can be seen as a sequence with a leader peptide-prepeptide. The latter suffers then a series of posttranslationally reactions to give place to a mature peptide without the leader peptide. 
 
 [![lasso.png](https://s4.postimg.org/x8itx6pbh/lasso.png)](https://postimg.org/image/4vnc6q3l5/)
 
 So, how do we do the cleave site prediction?
 Below one can see an example of part of an alignment of different lassopeptides precursor sequences. Do you see a pattern? ^^
 
 [![all_prec_2638.png](https://s3.postimg.org/ubptxdn3n/all_prec_2638.png)](https://postimg.org/image/a4ce52pm7/)
 
 What can we do with this? We can create profiles based on probabilistics - [Hidden Markov Models (HMM)](https://en.wikipedia.org/wiki/Hidden_Markov_model)
 
 After a good HMM profile, the plugin searches for sequences with hits for these profile. As consequence, we can predict where the cleavage site is and subsequently, identify the leader sequence and the core peptide.
 The same reasoning is apllied to predict putative cleavages at the end of the core peptide.
 
 After that, the plugin is made to focus on the possible features. In the case of lassopeptides, we have the prediction of:

*the ring they form - macrolactam - to generate a lariat knot (that's why they are called **lasso**peptides)
*weight prediction
*number of disulfide bons
*class (which depends on the number of disulfide bonds)
*tail putative cut 

After al this magic, here it is an example of a result for a Lassopeptide.

[![lassoOutput.png](https://s4.postimg.org/eceuk3zod/lasso_Output.png)](https://postimg.org/image/tl4rxvtcp/)


See you next year GSoC!
Thank you for this amazing experience.
oh, and by the way, thanks for reading this :)

Cheers,
Mariana Nave


 
 
 
 
 