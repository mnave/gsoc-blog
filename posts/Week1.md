One week already gone.

During these days I read a bit more about Lassopeptides - one of the RiPP classes I'll work with.
I confess I got a bit lost about the HMMs and the rules' system, but despite having a few more questions, I believe this won't be a problem ^^.
Also studied the plugin system and read the code of Lantipeptides' module.
So these first days were much more about figuring thigs out than coding, as expected!
I guess I don't have much more to say right now.

See you next week!