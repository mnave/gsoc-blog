Hello all!

The sencond GSoC week was a bit weird... it was difficult for me to accept the "magic" behind the [Hidden Markov Models (HMM)](https://en.wikipedia.org/wiki/Hidden_Markov_model) :) But I survived and already generated a few HMM profiles.
So, these days I played with protein sequences. Between sequence alignments I was able to identify patterns and generate different HMMs for class II lassos. 
I also started testing them, running genomes with already characterized lassos and comparing the scores > Still working on this; I can improve them!
To run those tests.. well, I already started playing with code. I started adapting the lantipeptides' module to perform these tests (yes, still have a lot of prints along the code ^^)
This next week I'll focus on class I lassos (HMM profile: generation and test) and I'll continue gathering more genomes of organisms that are known producers of lasso peptides.
I will also continue to tune the thresholds necessary for an accurate prediction and, of course, I'll start contributing to antiSMASH with code ^^

I guess this is all for now.
Let's code!


