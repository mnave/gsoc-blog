Hey hey

I'm a bit late for this post I guess. I'll try to write a bit more from now on.
This week I think I almost finished the plugin for lassopeptides prediction. 
Funny work! Probably it will need a few touches here and there, but almost there!
Lassopeptides are so cool! Try to google them ^^
So, imagine a lassopeptide:

				leader          lasso
		   N----------------|----------C
		   
		   
They indeed have a "lasso" after the cleavage of the leader sequence. So, I started by the identification of a pattern to predict the cleavage site (week 2). After that, the number of disulfide bonds, molecular weight and monoisotopic mass is calculated. 
Some of this peptides, also have a cleavage site at the C-terminal. 

                           		
							        
				leader        lasso 
		   N----------------|--------|-C
                            ^        ^

This part was a bit more tricky. The number of published lassos with cleaved off residues before isolation is small, the cleaved off sequences vary in length, there is no apparent pattern preceding the cut...
Consequently, generation of a pHMM was a bit harder and the threshold definition... well, wasn't easier. But it's working and I believe with reasonable accuracy ^^


Until Sunday, I'll try to see if there is something I can improve in this plugin. I'll continue talking with my mentors which are great (!) and I think I'll start studying another RiPP class.

Cheers!


