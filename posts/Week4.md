Hey hey!

Let me start by saying I just passed my midterm evaluation. Thank you ^^

About this 4th week, the pace was totally different. With the lassopeptides plugin almost done (probably I'll just see any other fine tunning in the end) I started reading about another class of RiPPs.
Have you ever heard about **Thiopeptides**?

Similar to lassopeptides, they are so great and have a huuuuge potential!
They are a bit more complex in terms of architecture, with rings and lots of posttranslational modifications.
Okay, so what do they have that makes them so awesome and interesting?
Thiopeptides have antibacterial properties... some of them with activity comparable to that of penicilins! Until now, dozens have been isolated from both marine and terrestrial Gram positive bacteria.
Among other nice properties, these secondary metabolites:

* inhibit protein synthesis in Gram + bacteria
						
* purported to be potent inhibitors of prokaryotic-like translational machinery within the apicoplast of
the malaria parasite (Plasmodium falciparum).
			
* have reported anticancer activity
			
impressive hun?

Well, next post will be less theoretical ^^
Cheers!
			
		
		