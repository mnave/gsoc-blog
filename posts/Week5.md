Hello you!

Okay, so I already have the plugin for thiopeptides running but didn't focus (yet!) on some predictions like molecular weight or extra modifications. Why?
Thiopeptides are a bit more tricky. There is no straightfoward way to predict their series (think like we're talking about different classes - these guys have 5: from _a_ to _d_) or how many residues are unmodified.
A challenge I found with these molecules: prediction of the cleavage site. As lassopeptides, thiopeptides also have a leader sequence that must be cleaved out from the core. I was able to play with different sequence alignments, but every single HMM profile I was able to build, was still allowing the detection of lots of "junk" sequences. For example:
	
All thiopeptides identified so far have Ser/Thr rich tails, like this:

	SCTTCVCTCSCC

By simply applying an HMM for the prediction of the cleavage site, I was catching quite different sequences, such as 

    LGSVSTFSSGSCPGSTVNTVSTASCQG
	
Not even close, right?

To overcome this problem, I tried to use a regular expression that matches the beginning of ALL the so far described thiopeptides .. and it works!
Plus, I also defined another "filter" based on the Ser/Thr content of the core peptide. Need to do a few more evaluations, but I believe I'm on the right way ^^

I'll be back to you soon!