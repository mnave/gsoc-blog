Hi there!


Weeks 6 and 7 were a bit complicated. Why?
lots of hours around the same question: How can I improve the cleavage site prediction?
In addition to the "main" HMM profile (which includes residues from both leader and core peptides), I had to try different methods to get an accurate prediction. 
So far, and not sure if I'll have to change more things, I also have pHMM for the cores (to "validate" the first prediction) and a regular expression to confirm the classic pattern of these peptides. I also try to filter by core size to avoid an incredible number of "junk" sequences that insisted in appear ^^. 

Here are a few examples of core sequences:

SCTTCECSCSCSS

VSSASCTTCICTCSCSS

IASASCTTCICTCSCSS

SNCTSTGTPASCCSCCCC

STNCFCYICCSCSSN

Can you see a simple and beautiful pattern?.. exactly :D

yeap... this was a tough cookie to crack. 


After this Lusitanian Odyssey I started to work around the thiopeptides' series. As I said in [week 5](https://bitbucket.org/mnave/gsoc-blog/src/c7b1bd196514e9e6ef202f1bed853556774a74c8/posts/Week5.md?at=master&fileviewer=file-view-default) post, there are 5 different series.
Identify series is impossible since they can only be determined from the oxidation state of the final product

What have we decided to do? 

We decided to divide thiopeptides in three different types, according to the presence of some biosynthetically specific genes in the cluster, relevant to certain structural features of the final product.

Briefly:

    Type I: Featuring genes encoding indolic acid side ring (series e)

    Type II: Featuring the gene coding for quinalic acid moiety formation (series a&b, c)

    Type III: No genes for synthesizing L-trp derivatives (series d)


So yesterday I started with weights prediction and guess what? After a few differences in weights I decided to go back to literature to discover that **some** thiopeptides also have the **last** residue removed.
After some not-that-light reading I thought I find a way to say: ok, if you are a type _x_ thiopeptide, then the probability of having your last residue cut off is high > let's cut it! erm... Not that easy. Weights prediction will have to wait a bit more.



Cheers ^^
	
