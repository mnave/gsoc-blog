Hi all!

Well.. evaluating my posts here I would say it is pretty clear I am not a blog person. ^^
If you're reading this, I'm sorry.

In my last [post](https://bitbucket.org/mnave/gsoc-blog/src/11514ee0ca26f2502cdfb43d9831ae208d3ba135/posts/Week6&7.md?at=master&fileviewer=file-view-default) I was working on the C-terminal cut of some thiopeptides.
During this task, I stumbled in small different problems so in addition to this cleavage prediction, I had to improve all the predictions I was doing till that day. 

So after this, weights prediction became a problem.
> How can we predict the molecular weight of a molecule that suffers so many different reactions?

cyclodehydrations and subsequent dehydrogenations to form azoles, dehydrations to generate dehydroaminoacids, intramolecular cyclization to afford the nitrogen heterocycle and we can also find methylations (and I bet I'm forgetting some!).

So my first approach was trying to understand all the chemical reactions that happen for one example and see if there are some "rules"/patterns to be applied to the remaining peptides.
It was difficult but I managed to do that for one thiopeptide. However, there are no peptides that suffer the same exact reactions, which is easy to understand since they are similar but not equal to each other. So a cys for thiopeptide A may not suffer the same reaction that a cys in thiopeptide B. We gotta love biochemistry ^^

I tried different approaches, including the identification of the enzymes' families for each cluster.
After lots of reading and try-errors I decided to keep it simple: we have 5 series of thiopeptides, which can be divided into 3 different types according to the central rings. 

> type I  : addition of an indolic acid

> type II : addition of a quinaldic acid


So, assuming that is impossible to determine an accurate value for the molecular weight without the exact enzyme acting on the exact element(s) of a clearly identified residue, I'm now trying to commit with a <5% error. It's working for some. Let's see what else can be done.


Cheers,

Mariana
