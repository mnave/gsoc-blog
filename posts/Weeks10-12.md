Hello there!

In these last weeks my work was basically finishing the tests. I really read dozens of papers and tried different ways to get an accurate weight prediction.. but no significant improvements were achieved.
The complexity of the thiopeptides' biosynthesis is so amazing!  

An example

[![13post.png](https://s3.postimg.org/yi4yliw77/13post.png)](https://postimg.org/image/4dgi05r3z/)

This is a scheme of the thiocillin gene cluster from B. cereus ATCC 14579 - 24 genes responsible for the production of the thiocillins.
The last 14 residues undergo 13 posttranslational modiﬁcations of 6 varieties to become
the mature thiocillins. Gray squares indicate the R groups in the thiocillin family members.

13 maturation reactions for these buddies, different R groups. Can you imagine the variety? Amazing molecules.

I'll be back with a summary.

